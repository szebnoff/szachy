#include "chessboard.h"
#include "graphics.h"
#include "knight.h"
#include "pawn.h"
#include "rook.h"
#include "bishop.h"
#include "queen.h"
#include "king.h"
#include "sound.h"

#include "input.h"

#include <SDL2/SDL.h>

#include <vector>
#include <algorithm>
#include <iostream>
#include <typeinfo>

namespace {
    const int SQUARE_SIZE = 64;
    const int BLACK_R = 181;
    const int BLACK_G = 136;
    const int BLACK_B = 99;
    const int WHITE_R = 240;
    const int WHITE_G = 217;
    const int WHITE_B = 181;

    const int EIGHT_RANK = 0;
    const int SEVENTH_RANK = 1;
    const int SECOND_RANK = 6;
    const int FIRST_RANK = 7;

    const int A_FILE = 0;
    const int B_FILE = 1;
    const int C_FILE = 2;
    const int D_FILE = 3;
    const int E_FILE = 4;
    const int F_FILE = 5;
    const int G_FILE = 6;
    const int H_FILE = 7;
}

Chessboard::Chessboard(Graphics &graphics)
{
    this->generateBackground();
    this->placePieces(graphics);
}

void Chessboard::generateBackground()
{
    for (int y = 0; y < 8; y++) {
        for (int x = 0; x < 8; x++) {
            SDL_Rect destinationRect = { x*SQUARE_SIZE, y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE };

            if( (y+x) % 2 )
                _blackSquares.push_back(destinationRect);
            else
                _whiteSquares.push_back(destinationRect);
        }
    }
}

void Chessboard::placePieces(Graphics &graphics)
{
    for(int i = 0; i < 8; i++) {
        this->_pieces.push_back(new Pawn(graphics, "WhitePawn.png", "white", i, SECOND_RANK));
        this->_pieces.push_back(new Pawn(graphics, "BlackPawn.png", "black", i , SEVENTH_RANK));
    }

    this->_pieces.push_back(new Rook(graphics, "WhiteRook.png", "white", A_FILE, FIRST_RANK));
    this->_pieces.push_back(new Rook(graphics, "WhiteRook.png", "white", H_FILE, FIRST_RANK));
    this->_pieces.push_back(new Rook(graphics, "BlackRook.png", "black", A_FILE, EIGHT_RANK));
    this->_pieces.push_back(new Rook(graphics, "BlackRook.png", "black", H_FILE, EIGHT_RANK));

    this->_pieces.push_back(new Knight(graphics, "WhiteKnight.png", "white", B_FILE, FIRST_RANK));
    this->_pieces.push_back(new Knight(graphics, "WhiteKnight.png", "white", G_FILE, FIRST_RANK));
    this->_pieces.push_back(new Knight(graphics, "BlackKnight.png", "black", B_FILE, EIGHT_RANK));
    this->_pieces.push_back(new Knight(graphics, "BlackKnight.png", "black", G_FILE, EIGHT_RANK));

    this->_pieces.push_back(new Bishop(graphics, "WhiteBishop.png", "white", C_FILE, FIRST_RANK));
    this->_pieces.push_back(new Bishop(graphics, "WhiteBishop.png", "white", F_FILE, FIRST_RANK));
    this->_pieces.push_back(new Bishop(graphics, "BlackBishop.png", "black", C_FILE, EIGHT_RANK));
    this->_pieces.push_back(new Bishop(graphics, "BlackBishop.png", "black", F_FILE, EIGHT_RANK));

    this->_pieces.push_back(new Queen(graphics, "WhiteQueen.png", "white", D_FILE, FIRST_RANK));
    this->_pieces.push_back(new Queen(graphics, "BlackQueen.png", "black", D_FILE, EIGHT_RANK));

    this->_pieces.push_back(new King(graphics, "WhiteKing.png", "white", E_FILE, FIRST_RANK));
    this->_pieces.push_back(new King(graphics, "BlackKing.png", "black", E_FILE, EIGHT_RANK));
}

void Chessboard::update(Input &input, Sound &sound)
{
    if(input.wasButtonPressed(SDL_BUTTON_LEFT)) {
        Piece *clickedPiece = this->checkCollisions(input);

        if(clickedPiece != nullptr && clickedPiece->getColor() == this->_colorToMove) {
            this->_holdedPiece = clickedPiece;
            this->_holdedPiece->isBeingHold = true;
        }
    }

    if(input.isButtonHeld(SDL_BUTTON_LEFT) && this->_holdedPiece != nullptr) {
        int mouseX, mouseY;
        input.getMousePosition(mouseX, mouseY);
        _holdedPiece->setScreenPosition(mouseX - SQUARE_SIZE/2, mouseY - SQUARE_SIZE/2);
    }

    if(input.wasButtonReleased(SDL_BUTTON_LEFT) && this->_holdedPiece != nullptr) {
        int mouseX, mouseY;
        input.getMousePosition(mouseX, mouseY);
        int boardX = mouseX / SQUARE_SIZE, boardY = mouseY / SQUARE_SIZE;

        if(this->_holdedPiece->isMoveLegit(boardX, boardY, *this)) {

            Piece *pieceToRemove = this->getPiece(boardX, boardY);
            if(pieceToRemove != nullptr && !pieceToRemove->isBeingHold) {
                this->removePiece(pieceToRemove);
                sound.playSound("strikeMove.wav");
            }
            else
                sound.playSound("normalMove.wav");


            std::string pieceType = typeid(this->_holdedPiece).name();
            std::cout << pieceType;

            this->_holdedPiece->setBoardPosition(boardX, boardY, *this);

            this->_colorToMove = (this->_colorToMove == "white") ? "black" : "white";
        }



        _holdedPiece->isBeingHold = false;
        _holdedPiece = nullptr;
    }
}

void Chessboard::draw(Graphics &graphics)
{
    this->drawBackground(graphics);
    for(auto&& piece : this->_pieces) {
        if(!piece->isBeingHold)
            piece->draw(graphics);
    }
    //draw holded piece last
    if(this->_holdedPiece != nullptr)
        this->_holdedPiece->draw(graphics);
}

void Chessboard::drawBackground(Graphics &graphics)
{
    graphics.drawSquares(_blackSquares, BLACK_R, BLACK_G, BLACK_B);
    graphics.drawSquares(_whiteSquares, WHITE_R, WHITE_G, WHITE_B);
}

Piece *Chessboard::checkCollisions(Input &input)
{
    int mouseX, mouseY;
    input.getMousePosition(mouseX, mouseY);
    for (auto&& piece : this->_pieces) {
        int x, y;
        piece->getBoardPosition(x, y);
        if ( (mouseX > x*64)  && (mouseX < (x*64 +SQUARE_SIZE)) && (mouseY > y*64) && (mouseY < (y*64 + SQUARE_SIZE)))
            return piece;
    }

    return nullptr;
}

Piece *Chessboard::getPiece(int x, int y)
{
    for(auto&& piece : this->_pieces) {
        int boardX, boardY;
        piece->getBoardPosition(boardX, boardY);
        if(boardX == x && boardY == y)
            return piece;
    }
    return nullptr;
}

void Chessboard::removePiece(Piece *piece)
{
    auto position = std::find(this->_pieces.begin(), this->_pieces.end(), piece);
    if(position != this->_pieces.end())
        this->_pieces.erase(position);
}







