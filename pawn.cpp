#include "pawn.h"
#include "chessboard.h"
#include <cmath>

Pawn::Pawn(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y):
    Piece(graphics, filepath, color, x, y)
{
}

void Pawn::setBoardPosition(int x, int y, Chessboard &chessboard)
{
    Piece::setBoardPosition(x, y, chessboard);
    this->_wasMoved = true;

    if(y == 0 || y == 7) {

        chessboard.removePiece(this);
    }
}

bool Pawn::isMoveLegit(int x, int y, Chessboard &chessboard)
{
    if(chessboard.getPiece(x, y) && chessboard.getPiece(x, y)->getColor() == this->_color)
        return false;

    if(!chessboard.getPiece(x, y) && this->_x != x)  //moves diagonally/sideways without attacking piece
        return false;

    if(abs(this->_y - y) > 2)  //moves by 3 or more squares
        return false;

    if(this->_wasMoved && abs(this->_y - y) > 1)  //moves by more than 1 square after the first move
        return false;

    if( (this->_color == "white" && y > this->_y) || (this->_color == "black" && y < this->_y) )  //moves backwars
        return false;

    if( (this->_x == x && chessboard.getPiece(x, y) ))  //attacks forward
        return false;

    if( (this->_y == y && this->_x != x))  //moves sideways
        return false;

    if( abs(this->_y - y) > 1 && this->_x != x && !this->_wasMoved) //attack by moving 2 squares on first move
        return false;

    return true;
}
