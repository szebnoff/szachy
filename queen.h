#ifndef QUEEN_H
#define QUEEN_H
#include "piece.h"

class Queen : public Piece
{
public:
    Queen(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y);
    bool isMoveLegit(int x, int y, Chessboard &chessboard);
};

#endif // QUEEN_H
