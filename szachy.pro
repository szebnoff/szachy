TEMPLATE = app
CONFIG += console c++11 static staticlib
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS_RELEASE += -Os

SOURCES += main.cpp \
    graphics.cpp \
    game.cpp \
    chessboard.cpp \
    piece.cpp \
    pawn.cpp \
    rook.cpp \
    knight.cpp \
    bishop.cpp \
    queen.cpp \
    king.cpp \
    input.cpp \
    sound.cpp

LIBS += -L D:/SDL2/lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_mixer


INCLUDEPATH += D:/SDL2/include
INCLUDEPATH += D:/SDL2/SDL_image/include

HEADERS += \
    graphics.h \
    globals.h \
    game.h \
    chessboard.h \
    piece.h \
    pawn.h \
    rook.h \
    knight.h \
    bishop.h \
    queen.h \
    king.h \
    input.h \
    sound.h

