#include "piece.h"
#include "graphics.h"
#include "chessboard.h"

#include <memory>
#include <iostream>
#include <SDL2/SDL.h>

namespace {
    const int IMAGE_SIZE = 320;
    const int PIECE_SIZE = 64;
}

Piece::Piece(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y):
    _color(color),
    _x(x),
    _y(y),
    isBeingHold(false)
{
    _texture = SDL_CreateTextureFromSurface(graphics.getRenderer(), graphics.loadImage(filepath).get());
    if(this->_texture == NULL)
        std::cout << "Unable to load image";

    _sourceRectangle = { 0, 0, IMAGE_SIZE, IMAGE_SIZE };
}

void Piece::draw(Graphics &graphics)
{
    SDL_Rect destRect;

    if(this->isBeingHold) {
        destRect = { this->_screenX, this->_screenY, PIECE_SIZE, PIECE_SIZE };

    }
    else
        destRect = { this->_x*PIECE_SIZE, this->_y*PIECE_SIZE, PIECE_SIZE, PIECE_SIZE };
    graphics.blitTexture(this->_texture, &this->_sourceRectangle, &destRect);
}

void Piece::getBoardPosition(int &x, int &y)
{
    x = _x;
    y = _y;
}

void Piece::getScreenPosition(int &x, int &y)
{
    x = _screenX;
    y = _screenY;
}

void Piece::setScreenPosition(int x, int y)
{
    _screenX = x;
    _screenY = y;
}

bool Piece::wasMoved()
{
    return this->_wasMoved;
}

void Piece::setBoardPosition(int x, int y, Chessboard &chessboard)
{
    _x = x;
    _y = y;
}

bool Piece::isMoveLegit(int x, int y, Chessboard &chessboard)
{
    return true;
}

std::string Piece::getColor()
{
    return this->_color;
}
