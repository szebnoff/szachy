#ifndef KING_H
#define KING_H
#include "piece.h"

class King : public Piece
{
public:
    King(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y);
    bool isMoveLegit(int x, int y, Chessboard &chessboard);
    void setBoardPosition(int x, int y, Chessboard &chessboard);

    bool wasMoved();
private:
    bool _wasMoved = false;
};

#endif // KING_H
