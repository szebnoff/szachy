#ifndef CHESSBOARD_H
#define CHESSBOARD_H

#include <vector>
#include <string>

struct SDL_Surface;
struct SDL_Renderer;
struct SDL_Rect;

class Graphics;
class Knight;
class Rook;
class Pawn;
class Piece;
class Input;
class Sound;

class Chessboard
{
public:
    Chessboard(Graphics &graphics);

    void draw(Graphics &graphics);
    void generateBackground();
    void placePieces(Graphics &graphics);
    void update(Input &input, Sound &sound);
    void removePiece(Piece *piece);

    Piece* getPiece(int x, int y);
private:
    std::vector<SDL_Rect> _blackSquares;
    std::vector<SDL_Rect> _whiteSquares;
    std::vector<Piece*> _pieces;

    void drawBackground(Graphics &graphics);
    Piece* checkCollisions(Input &input);

    Piece* _holdedPiece = nullptr;

    std::string _colorToMove = "white";
};

#endif // CHESSBOARD_H
