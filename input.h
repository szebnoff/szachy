#ifndef INPUT_H
#define INPUT_H

#include <map>
#include <SDL2/SDL_events.h>

class Input
{
public:
    Input();

    void beginNewFrame();
    void buttonDownEvent(SDL_Event &event);
    void buttonUpEvent(SDL_Event &event);
    void getMousePosition(int &x, int &y);

    bool wasButtonPressed(int buttonId);
    bool wasButtonReleased(int buttonId);
    bool isButtonHeld(int buttonId);

private:
    std::map<int, bool> _pressedButtons;
    std::map<int, bool> _heldButtons;
    std::map<int, bool> _releasedButtons;
};

#endif // INPUT_H
