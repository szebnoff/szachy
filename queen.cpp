#include "queen.h"
#include "rook.h"
#include "bishop.h"

Queen::Queen(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y):
    Piece(graphics, filepath, color, x, y)
{
}

bool Queen::isMoveLegit(int x, int y, Chessboard &chessboard)
{
    bool validRook = Rook::isMoveLegit(this->_x, this->_y, this->_color, x, y, chessboard);
    bool validBishop = Bishop::isMoveLegit(this->_x, this->_y, this->_color, x, y, chessboard);

    return validRook || validBishop;
}
