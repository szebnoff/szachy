#include "rook.h"
#include "chessboard.h"

Rook::Rook(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y):
    Piece(graphics, filepath, color, x, y)
{
}

bool Rook::isMoveLegit(int x, int y, Chessboard &chessboard)
{
    if(chessboard.getPiece(x, y) && chessboard.getPiece(x, y)->getColor() == this->_color)
        return false;

    if( this->_x != x && this->_y != y )
        return false;

    return true;
}

bool Rook::isMoveLegit(int startX, int startY, std::string color, int x, int y, Chessboard &chessboard)
{
    if(chessboard.getPiece(x, y) && chessboard.getPiece(x, y)->getColor() == color)
        return false;

    if( startX != x && startY != y )
        return false;

    return true;
}

bool Rook::wasMoved()
{
    return this->_wasMoved;
}

void Rook::setBoardPosition(int x, int y, Chessboard &chessboard)
{
    Piece::setBoardPosition(x, y, chessboard);
    this->_wasMoved = true;
}
