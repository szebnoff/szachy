#include "sound.h"

#include <SDL2/SDL_mixer.h>

Sound::Sound()
{
    Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048);
}

Mix_Chunk* Sound::getSound(const std::string filepath)
{
    if (!this->_sounds.count(filepath)) {
        this->_sounds[filepath] = Mix_LoadWAV(filepath.c_str());
    }
    return this->_sounds[filepath];
}

void Sound::playSound(const std::string filepath)
{
    Mix_PlayChannel(-1, this->getSound(filepath), 0);
}
