#ifndef GRAPHICS_H
#define GRAPHICS_H

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;
struct SDL_Rect;
struct SDL_Surface;

#include <string>
#include <vector>
#include <map>
#include <memory>

class Graphics
{
public:
    Graphics(int width, int height, const char title[]);

    //void blitSurface(SDL_Surface *texture, SDL_Rect *sourceRectangle, SDL_Rect *destinationRectangle);
    void blitTexture(SDL_Texture *texture, SDL_Rect *sourceRectangle, SDL_Rect *destinationRectangle);
    void flip();
    void clear();
    void drawSquares(std::vector<SDL_Rect> squares, int r, int g, int b);

    std::shared_ptr<SDL_Surface> loadImage(const std::string &filepath);
    SDL_Renderer *getRenderer();

private:
    SDL_Window *_window;
    SDL_Renderer *_renderer;
    std::map<std::string, std::shared_ptr<SDL_Surface>> _images;
};

#endif // GRAPHICS_H
