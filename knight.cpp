#include "knight.h"
#include "chessboard.h"

#include <cmath>

Knight::Knight(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y):
    Piece(graphics, filepath, color, x, y)
{
}

bool Knight::isMoveLegit(int x, int y, Chessboard &chessboard)
{
    if(chessboard.getPiece(x, y) && chessboard.getPiece(x, y)->getColor() == _color)
        return false;

    if( abs(this->_y - y) == 2 && abs(this->_x - x) == 1)
        return true;

    if( abs(this->_x - x) == 2 && abs(this->_y - y) == 1)
        return true;

    return false;
}
