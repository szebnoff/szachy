#ifndef GAME_H
#define GAME_H


class Game
{
public:
    Game();

private:
    void gameLoop();
};

#endif // GAME_H
