#include "king.h"
#include "chessboard.h"
#include "rook.h"

#include <cmath>
#include <iostream>
#include <memory>

King::King(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y):
    Piece(graphics, filepath, color, x, y)
{
}

bool King::isMoveLegit(int x, int y, Chessboard &chessboard)
{
    if(chessboard.getPiece(x, y) && chessboard.getPiece(x, y)->getColor() == this->_color)
        return false;

    if( abs(this->_y - y) > 1)
        return false;

    if( abs(this->_x - x) > 1 && this->wasMoved())
        return false;

    //castle
    if( !this->_wasMoved && this->_y == y && abs(this->_x - x) == 2) {
        int rookX = (this->_x > x) ? 0 : 7;
        Rook *rook = dynamic_cast<Rook*>(chessboard.getPiece(rookX, this->_y));
        if( !rook || rook->wasMoved() )
            return false;
    }

    return true;
}

void King::setBoardPosition(int x, int y, Chessboard &chessboard)
{
    if(!this->_wasMoved && abs(this->_x - x) == 2) {
        std::cout << "castle";
        if(this->_x > x) //queen side
            chessboard.getPiece(0, this->_y)->setBoardPosition(3, this->_y, chessboard);
        else //king side
            chessboard.getPiece(7, this->_y)->setBoardPosition(5, this->_y, chessboard);
    }

    Piece::setBoardPosition(x, y, chessboard);
    this->_wasMoved = true;
}

bool King::wasMoved()
{
    return this->_wasMoved;
}
