#include "input.h"
#include <SDL2/SDL.h>

Input::Input()
{
}

void Input::beginNewFrame()
{
    this->_pressedButtons.clear();
    this->_releasedButtons.clear();
}

void Input::buttonDownEvent(SDL_Event &event)
{
    int buttonId = event.button.button;
    this->_pressedButtons[buttonId] = true;
    this->_heldButtons[buttonId] = true;
}

void Input::buttonUpEvent(SDL_Event &event)
{
    int buttonId = event.button.button;
    this->_heldButtons[buttonId] = false;
    this->_releasedButtons[buttonId] = true;
}

void Input::getMousePosition(int &x, int &y)
{
    SDL_GetMouseState(&x, &y);
}

bool Input::isButtonHeld(int buttonId)
{
    return this->_heldButtons[buttonId];
}

bool Input::wasButtonPressed(int buttonId)
{
    return this->_pressedButtons[buttonId];
}

bool Input::wasButtonReleased(int buttonId)
{
    return this->_releasedButtons[buttonId];
}
