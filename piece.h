#ifndef PIECE_H
#define PIECE_H
#include <SDL2/SDL.h>
#include <string>
#include <vector>

class Graphics;
class Chessboard;

struct SDL_Texture;

class Piece
{
public:
    Piece(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y);
    void draw(Graphics &graphics);
    bool isBeingHold;
    void getBoardPosition(int &x, int &y);
    void getScreenPosition(int &x, int &y);
    void setScreenPosition(int x, int y);
    virtual void setBoardPosition(int x, int y, Chessboard &chessboard);
    virtual bool isMoveLegit(int x, int y, Chessboard &chessboard);
    std::string getColor();
    virtual bool wasMoved();
private:
    SDL_Texture *_texture;
    SDL_Rect _sourceRectangle;
    bool _wasMoved = false;
protected:
    const std::string _color;
    int _x, _y;
    int _screenX, _screenY;
};

#endif // PIECE_H

