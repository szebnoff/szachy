#ifndef ROOK_H
#define ROOK_H
#include "piece.h"

class Rook : public Piece
{
public:
    Rook(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y);
    bool isMoveLegit(int x, int y, Chessboard &chessboard);

    static bool isMoveLegit(int startX, int startY, std::string color, int x, int y, Chessboard &chessboard);
    bool wasMoved();

    void setBoardPosition(int x, int y, Chessboard &chessboard);
private:
    bool _wasMoved = false;
};

#endif // ROOK_H
