#ifndef SOUND_H
#define SOUND_H

#include <map>
#include <string>

struct Mix_Chunk;

class Sound
{
public:
    Sound();
    Mix_Chunk* getSound(const std::string filepath);

    void playSound(const std::string filepath);
private:
    std::map<std::string, Mix_Chunk*> _sounds;

};

#endif // SOUND_H
