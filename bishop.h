#ifndef BISHOP_H
#define BISHOP_H
#include "piece.h"

class Bishop : public Piece
{
public:
    Bishop(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y);
    bool isMoveLegit(int x, int y, Chessboard &chessboard);
    static bool isMoveLegit(int startX, int startY, std::string color, int x, int y, Chessboard &chessboard);
};

#endif // BISHOP_H
