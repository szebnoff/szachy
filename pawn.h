#ifndef PAWN_H
#define PAWN_H
#include "piece.h"

class Pawn : public Piece
{
public:
    Pawn(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y);
    bool isMoveLegit(int x, int y, Chessboard &chessboard);
    void setBoardPosition(int x, int y, Chessboard &chessboard);
private:
    bool _wasMoved = false;
};

#endif // PAWN_H
