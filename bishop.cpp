#include "bishop.h"
#include "chessboard.h"

Bishop::Bishop(Graphics &graphics, const std::string &filepath, const std::string &color, int x, int y):
    Piece(graphics, filepath, color, x, y)
{
}

bool Bishop::isMoveLegit(int x, int y, Chessboard &chessboard)
{
    if(chessboard.getPiece(x, y) && chessboard.getPiece(x, y)->getColor() == this->_color)
        return false;

    if( abs(this->_y - y) != abs(this->_x - x))
        return false;

    return true;
}

bool Bishop::isMoveLegit(int startX, int startY, std::string color, int x, int y, Chessboard &chessboard)
{
    if(chessboard.getPiece(x, y) && chessboard.getPiece(x, y)->getColor() == color)
        return false;

    if( abs(startY - y) != abs(startX - x) )
        return false;

    return true;
}
