#include "game.h"
#include "globals.h"
#include "graphics.h"
#include "chessboard.h"
#include "input.h"
#include "sound.h"

#include <SDL2/SDL.h>
#include <algorithm>
#include <iostream>

namespace {
    const int GAME_WIDTH = 512;
    const int GAME_HEIGHT = 512;
    const char GAME_TITLE[] = "Szachy";
    const int FPS = 60;
}

Game::Game()
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS);
    this->gameLoop();
    SDL_Quit();
}

void Game::gameLoop()
{
    Graphics graphics(GAME_WIDTH, GAME_HEIGHT, GAME_TITLE);
    Chessboard chessboard(graphics);
    Input input;
    Sound sound;

    while (true) {
        auto startTicks = SDL_GetTicks();
        SDL_Event event;
        input.beginNewFrame();

        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT)
                return;

            if(event.type == SDL_MOUSEBUTTONDOWN) {
                input.buttonDownEvent(event);
            }
            if(event.type == SDL_MOUSEBUTTONUP) {
                input.buttonUpEvent(event);
            }

            if(input.isButtonHeld(SDL_BUTTON_LEFT)) {
                int x, y;
                input.getMousePosition(x, y);
                std::cout << x << " " << y << "\n";
            }

            if(input.wasButtonReleased(SDL_BUTTON_LEFT))
                std::cout << "------------------------------------\n";
        }

        chessboard.draw(graphics);

        graphics.flip();

        chessboard.update(input, sound);

        SDL_Delay( std::max( 0, 1000/FPS - (int)(SDL_GetTicks() - startTicks) ) );
    }
}
