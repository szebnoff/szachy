#include "graphics.h"
#include "globals.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <memory>

Graphics::Graphics(int width, int height, const char title[])
{
    SDL_CreateWindowAndRenderer(width, height, SDL_WINDOW_SHOWN, &this->_window, &this->_renderer);
    SDL_SetWindowTitle(this->_window, title);
    //SDL_SetWindowFullscreen(this->_window, SDL_TRUE);
}

void Graphics::blitTexture(SDL_Texture *texture, SDL_Rect *sourceRectangle, SDL_Rect *destinationRectangle )
{
    SDL_RenderCopy(this->_renderer, texture, sourceRectangle, destinationRectangle);
}

//void Graphics::blitSurface(SDL_Surface *surface, SDL_Rect *sourceRectangle, SDL_Rect *destinationRectangle)
//{
 //   SDL_Texture *texture = SDL_CreateTextureFromSurface(this->_renderer, surface);
   // this->blitTexture(texture, sourceRectangle, destinationRectangle);
//}

void Graphics::flip()
{
    SDL_RenderPresent(this->_renderer);
}

void Graphics::clear()
{
    SDL_SetRenderDrawColor(this->_renderer, 0, 0, 0, 255);
    SDL_RenderClear(this->_renderer);
}

void Graphics::drawSquares(std::vector<SDL_Rect> squares, int r, int g, int b)
{
    SDL_SetRenderDrawColor(this->_renderer, r, g, b, 255);
    SDL_RenderFillRects(this->_renderer, &squares[0], squares.size());
}

std::shared_ptr<SDL_Surface> Graphics::loadImage(const std::string &filepath)
{
    std::shared_ptr<SDL_Surface> surface(IMG_Load(filepath.c_str()), SDL_FreeSurface);
    return surface;
}


SDL_Renderer *Graphics::getRenderer() {
    return this->_renderer;
}
